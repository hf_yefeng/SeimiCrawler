package cn.wanghaomiao.seimi.http.selenium;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;

public abstract class DriverFactory {

    public static WebDriver get(String client) {

        BrowserVersion browserVersion = new BrowserVersion(null, null, null, 1);
        HtmlUnitDriver driver = new HtmlUnitDriver();    //java版浏览器, 无界面渲染

//        WebDriver driver = new ChromeDriver();    //Chrome浏览器
//
//        WebDriver driver = new FirefoxDriver();   //Firefox浏览器
//
//        WebDriver driver = new EdgeDriver();      //Edge浏览器
//
//        WebDriver driver = new InternetExplorerDriver();  // Internet Explorer浏览器
//
//        WebDriver driver = new OperaDriver();     //Opera浏览器
//
//        WebDriver driver = new PhantomJSDriver();   //PhantomJS

        return driver;
    }
}
