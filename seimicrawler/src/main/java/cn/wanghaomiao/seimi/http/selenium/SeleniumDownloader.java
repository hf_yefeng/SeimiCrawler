package cn.wanghaomiao.seimi.http.selenium;

import cn.wanghaomiao.seimi.core.SeimiDownloader;
import cn.wanghaomiao.seimi.def.BaseSeimiCrawler;
import cn.wanghaomiao.seimi.http.SeimiCookie;
import cn.wanghaomiao.seimi.struct.CrawlerModel;
import cn.wanghaomiao.seimi.struct.Request;
import cn.wanghaomiao.seimi.struct.Response;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.List;

/**
 * selenium框架下载器
 *
 * 教程:
 * https://www.cnblogs.com/tester-ggf/p/12602211.html
 */
public class SeleniumDownloader implements SeimiDownloader {

    private CrawlerModel crawlerModel;
    private Request currentRequest;

    public SeleniumDownloader(CrawlerModel crawlerModel) {
        this.crawlerModel = crawlerModel;
    }

    @Override
    public Response process(Request request) throws Exception {
        // todo
        // 获取驱动
        WebDriver webDriver = DriverFactory.get(null);
        // 2.打开首页
        webDriver.get(request.getUrl());

        // 3.检测是否加载完毕


        // 5.退出浏览器 做成一个工具类, 同意管理打开的浏览器
        // webDriver.quit();
        return null;
    }

    @Override
    public Response metaRefresh(String nextUrl) throws Exception {
        return null;
    }

    @Override
    public int statusCode() {
        return 0;
    }

    @Override
    public void addCookies(String url, List<SeimiCookie> seimiCookies) {

    }
}
